# Attack Graph GUI

## Installation with Docker

1. Install docker for [windows](https://docs.docker.com/docker-for-windows/) / [mac](https://docs.docker.com/docker-for-mac/)

2. Clone repo

3. Navigate to root directory

4. Start the docker containers:

    `docker-compose up`

5. Navigate to http://localhost:8000

*The first time docker is started, the containers must be built, which is time consuming.  Subsequent times will be much faster.*

## Installation without Docker

1. Verify you are running python 3

    `python --version`

2. If you are using any type of virtual env (such as `virtualenv` or `conda`), activate it.

2. Install django using pip

    `pip install django==2.2`

2. Clone repo

3. Navigate to root directory (where the `manage.py` file is located)

4. Start the django server

    `python manage.py runserver`

5. Navigate to http://localhost:8000
