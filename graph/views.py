from django.shortcuts import render, redirect
from django.shortcuts import get_list_or_404, get_object_or_404
from django.db.models import Q
from graph.models import Node, Arrow, Device, Connection
from graph.parser import parse_dot, parse_hacl
from graph.graph_gen import pydot

IMG_FILE_PATH = 'graph/static/img/'


def index(request):
    if request.method == 'GET':
        context = {}
        return render(request, 'graph/index.html', context)
    if request.method == 'POST':
        Node.objects.all().delete()
        Arrow.objects.all().delete()
        Device.objects.all().delete()
        Connection.objects.all().delete()

        hacl = request.POST.get('hacl')
        hacl = parse_hacl(hacl)
        for d in hacl:
            device = Device.objects.get_or_create(name=d['device'])[0]
            con = Device.objects.get_or_create(name=d['connected_to'])[0]
            protocol = None if d['protocol'] == '_' else d['protocol']
            port = None if d['port'] == '_' else d['port']
            connection = Connection(device=device,
                                    connected_to=con,
                                    protocol=protocol,
                                    port=port)
            connection.save()

        devices = Device.objects.all().values_list('name', flat=True)
        data = request.POST.get('data')
        data = parse_dot(data)
        for n in data['nodes']:
            node = Node(block=n['block'], label=n['label'], shape=n['shape'])
            for d in devices:
                if d in node.label:
                    node.device = Device.objects.filter(name=d).first()
            node.save()
        for a in data['arrows']:
            s = Node.objects.filter(block=a['start']).first()
            e = Node.objects.filter(block=a['end']).first()
            arrow = Arrow(start_block=s, end_block=e)
            arrow.save()

        return redirect('graph:results')


def results(request):
    if request.method == 'GET':
        devices = Device.objects.all()
        context = {'devices': devices}
        data = {'nodes': {}, 'arrows': []}
        nodes = Node.objects.all()
        for node in nodes:
            arrows = Arrow.objects.filter(start_block=node)
            data['nodes'][node.block] = node
            for arrow in arrows:
                data['arrows'].append(arrow)
        pydot(data)
    return render(request, 'graph/results.html', context)


def device_results(request, device_filter):
    if request.method == 'GET':
        devices = Device.objects.all()
        context = {'device': device_filter, 'devices': devices}

        try:
            data = {'nodes': {}, 'arrows': []}
            device = get_object_or_404(Device, name=device_filter)
            nodes = get_list_or_404(Node, device=device)
            for node in nodes:
                data['nodes'][node.block] = node
                arrows = Arrow.objects.filter(
                    Q(start_block=node) | Q(end_block=node)).all()
                for arrow in arrows:
                    data['arrows'].append(arrow)
            for arrow in data['arrows']:
                if arrow.end_block.block not in data['nodes']:
                    data['nodes'][arrow.end_block.block] = arrow.end_block
            pydot(data)
        except:
            return render(request, 'graph/no_results.html', context)
        return render(request, 'graph/device_results.html', context)



















#
