import pydotplus


def pydot(data):
    nodes = data['nodes']
    arrows = data['arrows']
    graph = pydotplus.Dot(graph_type='digraph')
    graph_nodes = {}

    for key, value in nodes.items():
        label = value.label.replace(':', '-')
        node = pydotplus.Node(label, shape=value.shape)
        graph_nodes[key] = node
        graph.add_node(node)

    for arrow in arrows:
        start = arrow.start_block.block
        end = arrow.end_block.block

        edge = pydotplus.Edge(graph_nodes[start], graph_nodes[end])

        graph.add_edge(edge)

    graph.write_png('graph/static/graph/graph.png')
