from django.db import models
from django.contrib.auth.models import User
import datetime


class Device(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Connection(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE, related_name='device')
    connected_to = models.ForeignKey(Device, on_delete=models.CASCADE, related_name='connected_to')
    protocol = models.CharField(max_length=100, blank=True, null=True)
    port = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.device.name + ' -> ' + self.connected_to.name


class Node(models.Model):
    device = models.ForeignKey(Device, blank=True, null=True, on_delete=models.CASCADE)
    block = models.SmallIntegerField()
    label = models.CharField(max_length=200)
    DIAMOND = 'diamond'
    ELLIPSE = 'ellipse'
    BOX = 'box'
    SHAPE_CHOICES = (
        (DIAMOND, 'diamond'),
        (ELLIPSE, 'ellipse'),
        (BOX, 'box'),
    )
    shape = models.CharField(
        max_length=10,
        choices=SHAPE_CHOICES,
    )

    def __str__(self):
        return self.label + ', ' + self.shape


class Arrow(models.Model):
    start_block = models.ForeignKey(Node, on_delete=models.CASCADE, related_name='start')
    end_block = models.ForeignKey(Node, on_delete=models.CASCADE, related_name='end')

    def __str__(self):
        return str(self.start_block.block) + ' -> ' + str(self.end_block.block)
