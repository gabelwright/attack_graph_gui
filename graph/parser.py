
def parse_dot(data):
    data = "".join(data.split())
    data = data[data.find('{')+1:data.find('}')]
    split_data = data.split(';')

    nodes = []
    arrows = []

    for i in split_data:
        if '->' in i:
            start = int(i[0:i.find('->')])
            end = int(i[i.find('->')+2:])
            arrow = {'start': start, 'end': end}
            arrows.append(arrow)
        elif i:
            block = int(i[0:i.find('[')])
            label = i[i.find('label="')+7:i.find('",')]
            shape = i[i.find('shape=')+6:i.find('"];')]
            node = {'block': block, 'label': label, 'shape': shape}
            nodes.append(node)

    ret = {'arrows': arrows, 'nodes': nodes}

    return ret


def parse_hacl(data):
    data = data.strip()
    data = data.split('\n')
    hacl = []

    for d in data:
        d = d.replace('hacl(', '')
        d = d.replace(').', '')
        d = "".join(d.split())
        d = d.split(',')
        device = {
            'device': d[0],
            'connected_to': d[1],
            'protocol': d[2],
            'port': d[3]
        }
        hacl.append(device)

    return hacl
