from . import views
from django.conf.urls import url
from django.urls import path

app_name = 'graph'
urlpatterns = [
    path('', views.index, name='index'),
    url(r'^results/$', views.results, name='results'),
    url(r'^results/(?P<device_filter>[a-zA-Z0-9\-_]+)$',
        views.device_results,
        name='device_results'),
]
