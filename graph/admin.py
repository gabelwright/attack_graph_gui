from django.contrib import admin
from .models import Node, Arrow, Device, Connection

admin.site.register(Node)
admin.site.register(Arrow)
admin.site.register(Device)
admin.site.register(Connection)
